﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Collections;

public class LobbyMenu : MonoBehaviour {
    string playerName;
    [SerializeField] GameObject namePanel;
    [SerializeField] GameObject lobbyPanel;
    public GameObject lobbyPlayerList;
    public UnityEngine.UI.Text debugText;

    void Start()
    {
        playerName = PlayerPrefs.GetString("PlayerName");
        if(playerName == "")
        {
            ShowNameInput();
        }
        if (NetworkLobby.s_Singleton.localPlayer != null) {
            debugText.text = NetworkLobby.s_Singleton.localPlayer.name;
        }
    }

    public void HostGame()
    {
        NetworkLobby.s_Singleton.AttemptHost();
    }

    public void FindGame()
    {
        NetworkLobby.s_Singleton.AttemptClient();
    }

    public void ShowNameInput()
    {
        NetworkLobby.s_Singleton.showLobbyGUI = false;
        NetworkLobby.s_Singleton.GetComponent<NetworkManagerHUD>().showGUI = false;
        namePanel.SetActive(true);
    }

    public void SetNameInput()
    {
        UnityEngine.UI.InputField nameInput = GameObject.Find("InputField_Name").GetComponent<InputField>();
        NetworkLobby.s_Singleton.showLobbyGUI = true;
        NetworkLobby.s_Singleton.GetComponent<NetworkManagerHUD>().showGUI = true;
        PlayerPrefs.SetString("PlayerName", nameInput.text);
        namePanel.SetActive(false);
    }

    public void ShowLobby()
    {
        lobbyPanel.SetActive(true);
    }

    public void HideLobby()
    {
        lobbyPanel.SetActive(false);
    }

    public void LeaveLobby()
    {
        NetworkLobby.s_Singleton.LeaveLobby();
        HideLobby();
    }

    public void AllowReady()
    {
        Debug.Log("Allowing ready.");
        UnityEngine.UI.Toggle toggle = GameObject.Find("ToggleReady").GetComponent<UnityEngine.UI.Toggle>();
        toggle.GetComponent<ToggleHelper>().SetInteractable(true);
    }
    public void DisableReady()
    {
        Debug.Log("Disabling Ready");
        if (SceneManager.GetActiveScene().name == "Lobby")
        {
            Debug.Log("Current scene is: " + SceneManager.GetActiveScene().name);
            GameObject toggleReady = GameObject.Find("ToggleReady");
            Debug.Log("Toggle ready is: " + toggleReady);
            if (toggleReady)
            {
                UnityEngine.UI.Toggle toggle = toggleReady.GetComponent<UnityEngine.UI.Toggle>();
                Debug.Log("Toggle is: " + toggle);
                toggle.GetComponent<ToggleHelper>().SetInteractable(false);
            }
        }
    }

    public void AddLobbyAvatar(GameObject avatar)
    {
        avatar.transform.SetParent(lobbyPlayerList.transform);
    }

    public void ToggleReady()
    {
        UnityEngine.UI.Toggle toggle = GameObject.Find("ToggleReady").GetComponent<UnityEngine.UI.Toggle>();
        if (toggle.isOn)
        {
            Debug.Log("READY!");
        }
        else
        {
            Debug.Log("NOT READY!");
        }
        NetworkLobby.s_Singleton.localPlayer.GetComponent<LobbyPlayer>().ToggleReady(toggle.isOn);
    }
}
