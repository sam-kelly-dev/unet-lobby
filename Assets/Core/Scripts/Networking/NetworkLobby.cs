﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Collections;

public class MyMsgTypes
{
    public static short MSG_LOGIN_RESPONSE = 1000;
    public static short MSG_SCORE = 1005;
    public static short MSG_READY_AVAILABLE = 1010;
}

public class ReadyAvailableMessage : MessageBase
{
    public bool readyAvailable;
}

public class NetworkLobby : NetworkLobbyManager {
    public static NetworkLobby s_Singleton;
    public GameObject localPlayer;

    public int localNumPlayers = 0;

    void Awake()
    {
        if (s_Singleton != null) {
            Destroy(gameObject);
            return;
        }
        else
        {
            s_Singleton = this;
        }
    }
    
    public override void OnLobbyStartHost()
    {
        //Debug.Log("OnLobbyStartHost");
        base.OnLobbyStartHost();
    }

    public string getNetworkSceneName()
    {
        return networkSceneName;
    }

    public override void OnLobbyStopHost()
    {
        //Debug.Log("Host stopping.");
        base.OnLobbyStopHost();
    }

    public override void OnLobbyServerConnect(NetworkConnection conn)
    {

        if (localPlayer)
        {
            LobbyPlayer lp = localPlayer.GetComponent<LobbyPlayer>();
            if (lp)
            {
                lp.ServerUpdatePlayerCount();
            }
        }
        base.OnLobbyServerConnect(conn);
    }

    public override void OnLobbyServerDisconnect(NetworkConnection conn)
    {
        //Debug.Log("Server registered disconnect: " + conn.connectionId + " has objects " + conn.playerControllers.Count);
        base.OnLobbyServerDisconnect(conn);
        if (localPlayer)
        {
            LobbyPlayer lp = localPlayer.GetComponent<LobbyPlayer>();
            if (lp)
            {
                lp.ServerUpdatePlayerCount();
            }
        }
    }

    public override void OnLobbyServerPlayerRemoved(NetworkConnection conn, short playerControllerId)
    {
        base.OnLobbyServerPlayerRemoved(conn, playerControllerId);
        if (localPlayer)
        {
            LobbyPlayer lp = localPlayer.GetComponent<LobbyPlayer>();
            if (lp)
            {
                lp.ServerUpdatePlayerCount();
            }
        }
    }

    public override void OnLobbyStartClient(NetworkClient lobbyClient)
    {
        //Debug.Log("------------------------------------STARTING CLIENT!----------------------------------");
        base.OnLobbyStartClient(lobbyClient);
    }

    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
    {
        if (localPlayer)
        {
            LobbyPlayer lp = localPlayer.GetComponent<LobbyPlayer>();
            if (lp)
            {
                lp.ServerUpdatePlayerCount();
            }
        }
        return base.OnLobbyServerCreateLobbyPlayer(conn, playerControllerId);
    }

    public override void OnLobbyStopClient()
    {
        base.OnLobbyStopClient();
        ClientScene.DestroyAllClientObjects();
        if (SceneManager.GetActiveScene().name != "Lobby")
        {
            //We stopped the client, but it left us in the "Game" scene. Go back to the lobby.
            SceneManager.LoadScene("Lobby");
        }
        else
        {
            GameObject canvas = GameObject.Find("Canvas");
            if (canvas)
            {
                LobbyMenu lobbyMenu = canvas.GetComponent<LobbyMenu>();
                if (lobbyMenu)
                {
                    lobbyMenu.HideLobby();
                }
            }
        }
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
    }

    public override void OnLobbyClientSceneChanged(NetworkConnection conn)
    {
        //Debug.Log("OnLobbyClientSceneChanged " + networkSceneName);
        base.OnLobbyClientSceneChanged(conn);
    }

    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
    { 
        Debug.Log("OnLobbyServerSceneLoadedForPlayer");
        //Assign the lobbyPlayer's data to the gamePlayer's data. We're on the server, so we can set the syncvar without calling a Cmd_Function from the authoritative player. 
        GamePlayer gp = gamePlayer.GetComponent<GamePlayer>();
        string s = lobbyPlayer.name.ToString();
        gp.ServerSetPlayerName(s);
        return true;
    }

    #region GUI Hooks
    public void AttemptHost()
    {
        networkSceneName = "";
        NetworkServer.SetAllClientsNotReady();
        ClientScene.DestroyAllClientObjects();
        NetworkLobby.s_Singleton.StartHost();
    }
    public void AttemptClient()
    {
        networkSceneName = "";
        NetworkServer.SetAllClientsNotReady();
        ClientScene.DestroyAllClientObjects();
        NetworkLobby.s_Singleton.StartClient();
    }
    public void LeaveLobby()
    {
        networkSceneName = "";
        NetworkLobby.s_Singleton.StopClient();
        NetworkLobby.s_Singleton.StopHost();
    }
    public void LeaveGame()
    {
        networkSceneName = "";
        NetworkLobby.s_Singleton.StopClient();
        NetworkLobby.s_Singleton.StopHost();
        
    }
    #endregion

    #region Game Hooks
    public void StartCountdown()
    {

    }
    public void EndGame()
    {
        //Back to lobby!
    }
    #endregion
}
