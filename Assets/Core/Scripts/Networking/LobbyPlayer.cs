﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Collections;


public class LobbyPlayer : NetworkLobbyPlayer
{
    LobbyMenu lobbyMenu;

    [SerializeField]
    GameObject lobbyAvatarPrefab;

    GameObject lobbyAvatar;

    [SyncVar(hook = "OnNameChange")]
    public string playerName;

    [SyncVar(hook = "OnPlayerCountChange")]
    public bool readyAllowed;

    public override void OnStartLocalPlayer()
    {
        //Debug.Log("OnStartLocalPlayer");
        base.OnStartLocalPlayer();
        NetworkLobby.s_Singleton.localPlayer = gameObject;
        ComputePlayerName();
    }

    protected void ComputePlayerName()
    {
        if (Application.isEditor)
        {
            Cmd_ChangeName("EditorPlayer");
            if (isLocalPlayer && isServer)
            {
                playerName = "EditorPlayer";
            }
        }
        else
        {
            Cmd_ChangeName(PlayerPrefs.GetString("PlayerName"));
            if (isLocalPlayer && isServer)
            {
                playerName = PlayerPrefs.GetString("PlayerName");
            }
        }

    }

    [Server]
    public void ServerUpdatePlayerCount()
    {        
        //Debug.Log("Server is updating the player count for the ready check stuff.");
        readyAllowed = NetworkLobby.s_Singleton.numPlayers >= NetworkLobby.s_Singleton.minPlayers;
    }

    void OnPlayerCountChange(bool n)
    {
        
        //Debug.Log("OnPlayerCountChange, readyAllowed: " + n + " minPlayers is: " + NetworkLobby.s_Singleton.minPlayers);
        readyAllowed = n;
        GameObject canvas = GameObject.Find("Canvas");
        if(readyAllowed == true)
        {
            canvas.GetComponent<LobbyMenu>().AllowReady();
        }
        else
        {
            LobbyMenu lobbyMenu = canvas.GetComponent<LobbyMenu>();
            if (lobbyMenu)
            {
                lobbyMenu.DisableReady();
            }
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    public override void OnClientEnterLobby()
    {
        //Debug.Log("OnClientEnterLobby, current network scene: " + NetworkLobby.s_Singleton.getNetworkSceneName() + ", actual scene: " + SceneManager.GetActiveScene().name );
        base.OnClientEnterLobby();
        if (playerName != "")
        {
            gameObject.name = playerName;
        }
        else
        {
            if (isServer || isLocalPlayer)
            {
                ComputePlayerName();
            }
        }

        //Debug.Log("Creating a Lobby Avatar: " + playerName);
        GameObject canvas = GameObject.Find("Canvas");
        lobbyMenu = canvas.GetComponent<LobbyMenu>();
        lobbyAvatar = (GameObject)GameObject.Instantiate(lobbyAvatarPrefab, Vector3.zero, Quaternion.identity);
        lobbyAvatar.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = playerName;
        lobbyAvatar.transform.Find("ReadyIndicator").gameObject.SetActive(readyToBegin);
        lobbyMenu.AddLobbyAvatar(lobbyAvatar);
        lobbyMenu.ShowLobby();

        if (isServer)
        {
            ServerUpdatePlayerCount();
        }
    }

    public override void OnClientExitLobby()
    {
        //Debug.Log("LobbyPlayer: Client exited lobby.");
        if (lobbyAvatar != null)
        {
            Destroy(lobbyAvatar);
        }
        base.OnClientExitLobby();
    }
    
    public override void OnNetworkDestroy()
    {
        if (lobbyAvatar != null)
        {
            //Debug.Log("Destroying Lobby Avatar.");
            Destroy(lobbyAvatar);
        }
        base.OnNetworkDestroy();
    }

    #region Name Functions
    [Command]
    void Cmd_ChangeName(string s)
    {
        playerName = s;
        gameObject.name = s;
        if (isLocalPlayer)
        {
            GameObject.Find("Canvas").GetComponent<LobbyMenu>().debugText.text = playerName;
        }
    }

    void OnNameChange(string s)
    {
        playerName = s;
        gameObject.name = s;
        if (lobbyAvatar)
        {
            lobbyAvatar.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = playerName;
        }
    }
    #endregion

    #region UI Hooks
    public void ToggleReady(bool r)
    {
        readyToBegin = r;
        if(readyToBegin == true)
        {
            SendReadyToBeginMessage();
        }
        else
        {
            SendNotReadyToBeginMessage();
        }
    }
    public override void OnClientReady(bool readyState)
    {
        //Debug.Log("LobbyPlayer: OnClientReady!");
        base.OnClientReady(readyState);
        lobbyAvatar.transform.Find("ReadyIndicator").gameObject.SetActive(readyState);
    }
    public void BroadcastReadyState()
    {
        ToggleReady(readyToBegin);
    }
    #endregion
}
