﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class GamePlayer : NetworkBehaviour {

    [SyncVar(hook = "OnNameChange")]
    public string playerName;
    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        NetworkLobby.s_Singleton.localPlayer = gameObject;
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (playerName != "")
        {
            DestroyLobbyCounterpart();
            gameObject.name = playerName;
        }
    }

    protected void DestroyLobbyCounterpart()
    {
        Debug.Log("Destroying lobby counterpart of " + playerName);
        GameObject go = GameObject.Find(playerName);
        LobbyPlayer lp = go.GetComponent<LobbyPlayer>();
        if (lp != null)
        {
            GameObject.Destroy(go);
        }
    }

    #region Name Functions
    public void ServerSetPlayerName(string s)
    {
        playerName = s;
    }

    [Command]
    void Cmd_ChangeName(string s)
    {
        playerName = s;
    }

    void OnNameChange(string s)
    {
        playerName = s;
        DestroyLobbyCounterpart();
        gameObject.name = s;
    }
    #endregion

    #region Team Functions

    #endregion

    #region Local Preferences

    #endregion
}
