# UNET LOBBY #

### What is this? ###

This is a quick example that should be helpful in understanding creating a simple multiplayer game using the NetworkLobbyManager. Right now, it's just for educational purposes (you and me both), and will run locally, without any errors or broken functionality or weirdness. Hopefully this is a more robust and functional example than the standard NetworkManagerHUD gives you.

There's no actual gameplay here, or movement syncing, or game-ish mechanics. This is simply a barebones lobby example to get you into and out of the game smoothly.

#### Quick Start ####

Make sure you've got [Unity3D 5.3.1p3](http://unity3d.com/unity/qa/patch-releases).

1. git clone https://bitbucket.org/sam-kelly-dev/unet-lobby.git
2. Open the project in Unity3D.
3. Open the "Lobby" scene.
4. Build & Run, open two instances of the player (or one player and one editor).

#### Configuration ####
I tried to be as minimal in my structure as possible. Everything is inside of /Core. Prefabs are in Core/Prefabs, Scenes are in Core/Scenes, Scripts are in Core/Scripts.

The bulk of the code lies in the NetworkLobby, LobbyPlayer, and GamePlayer scripts. The UI scripts are currently forced in there, but I'd like to decouple the UI from the actual functionality in the future.

This was built based on the [Unity3D documentation](http://docs.unity3d.com/Manual/class-NetworkLobbyManager.html) of NetworkLobbyManager and LobbyPlayer, Googling all the weird things I encountered, and lots of trial and error.

### Who do I talk to? ###

Find me on reddit - https://www.reddit.com/u/omg_ketchup/

Accepting pull requests as long as they don't introduce errors, and the code is clear enough to follow.